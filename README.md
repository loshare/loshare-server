# Location API

The Location API is a RESTful API that allows you to manage user locations. It provides endpoints to save user locations and retrieve the last saved location for a user.

## Requirements

- Node.js (version 14 or higher)
- npm (Node.js package manager)
- Docker (optional)

## Getting Started
### Running with npm
1. Clone the repository:

   ```shell
   git clone <repository_url>
   ```

2. Install dependencies:

   ```shell
   npm install
   ```

3. Start the server:

   ```shell
   npm start
   ```

4. The server will be running at `http://localhost:3000`.
### Running with Docker

1. Clone the repository:

   ```shell
   git clone <repository_url>
   ```

2. Build the Docker image:

   ```shell
   docker build -t location-api .
   ```

3. Run the Docker container:

   ```shell
   docker run -p 3000:3000 -d location-api
   ```

4. The server will be running inside the Docker container at `http://localhost:3000`.

## Logging

The server logs all incoming requests and responses to the log file `server.log`. To view the log file, access the `/logs` endpoint of the server.

- `GET /logs`: View the log file.

## API Usage

- `POST /my_location`: Save user location. Requires a request body with a `userId` and `location` parameters.

- `GET /get_location/{userId}`: Get user location. Requires a `userId` path parameter.

- `GET /database`: Get the entire database of user locations.

### Save User Location

To save a user's location, make a POST request to `/my_location` with the following JSON body:

```json
{
  "userId": "user123",
  "location": [12.34, 56.78]
}
```

### Get User Location

To retrieve the last saved location for a user, make a GET request to `/get_location/{userId}`, where `{userId}` is the ID of the user you want to retrieve the location for.

### Get Database

To retrieve the entire database of user locations, make a GET request to `/database`.

## License

View the [license file](LICENSE).
