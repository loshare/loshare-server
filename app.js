const express = require('express');
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');
const YAML = require("yamljs");

const openApiDefinition = YAML.load('./openapi.yaml');

const app = express();

// Create a write stream for the log file
const logFilePath = path.join(__dirname, 'server.log');
const accessLogStream = fs.createWriteStream(logFilePath, { flags: 'a' });

// Logging middleware with file stream
app.use(morgan('combined', { stream: accessLogStream }));

// In-memory database to store user locations
const database = {};

// Middleware to parse JSON request bodies
app.use(express.json());

// Serve static files (index.html)
app.use(express.static('public'));

// Route to view log file
app.get('/logs', (req, res) => {
  fs.readFile(logFilePath, 'utf8', (err, data) => {
    if (err) {
      console.error(`Error reading log file: ${err.message}`);
      return res.status(500).send('Error reading log file');
    }
    res.type('text/plain').send(data);
  });
});


// Endpoint: /my_location
app.post('/my_location', (req, res) => {
  // Logic to save user location with the current time in the database
  // Access userId and location from req.body
  const { userId, location } = req.body;

  // Validate the location format
  if (!Array.isArray(location) || location.length !== 2) {
    console.log(location)
    return res.status(400).json({ error: 'Invalid location format, not array of size 2' });
  } else {
    console.log(location)
  }

  const [longitudeStr, latitudeStr] = location;

  // Validate the longitude and latitude as numbers
  const longitude = parseFloat(longitudeStr);
  const latitude = parseFloat(latitudeStr);

  // Check if the parsed values are valid numbers
  if (
      Number.isNaN(longitude) ||
      Number.isNaN(latitude) ||
      Math.abs(longitude) > 180 ||
      Math.abs(latitude) > 90
  ) {
    return res.status(400).json({ error: 'Invalid location format, too big' });
  }

  // Save the data in the database with the current timestamp
  const timestamp = new Date().toISOString();
  database[userId] = {
    location: [longitude, latitude],
    timestamp,
  };

  res.sendStatus(200);
});

// Endpoint: /get_location/{userId}
app.get('/get_location/:userId', (req, res) => {
  // Logic to retrieve the last saved location and time for the given userId
  // Access userId from req.params
  const { userId } = req.params;

  // Lookup the data in the database
  const userLocation = database[userId];

  if (userLocation) {
    // Return the result as JSON
    res.json({
      userId,
      location: userLocation.location,
      timestamp: userLocation.timestamp,
    });
  } else {
    res.sendStatus(404);
  }
});

// Endpoint: /database
app.get('/database', (req, res) => {
  // Return the entire database as JSON
  res.json(database);
});

// Start the server
const port = 3000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
  console.log(`http://localhost:${port}`);
});

