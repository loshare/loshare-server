// Function to validate longitude and latitude values
function isValidLongitude(longitude) {
    return /^-?\d+(\.\d+)?$/.test(longitude);
}

function isValidLatitude(latitude) {
    return /^-?\d+(\.\d+)?$/.test(latitude);
}

// Function to fetch the entire database and update the UI
function fetchDatabase() {
    fetch('/database')
        .then(response => response.json())
        .then(data => {
            const databaseDiv = document.getElementById('database');
            databaseDiv.innerHTML = '<pre>' + JSON.stringify(data, null, 2) + '</pre>';
        })
        .catch(error => {
            alert('An error occurred while fetching the database');
            console.error(error);
        });
}

document.getElementById('locationForm').addEventListener('submit', async (e) => {
    e.preventDefault();
    const userId = document.getElementById('userId').value;
    const longitude = document.getElementById('longitude').value;
    const latitude = document.getElementById('latitude').value;

    console.log("test")

    // Validate longitude and latitude
    if (!isValidLongitude(longitude) || !isValidLatitude(latitude)) {
        alert('Invalid location format');
        return;
    }

    try {
        const response = await fetch('/my_location', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                userId,
                location: [parseFloat(longitude), parseFloat(latitude)]
            })
        });

        console.log(JSON.stringify({
            userId,
            location: [parseFloat(longitude), parseFloat(latitude)]
        }))

        if (response.ok) {
            alert('Location saved successfully');
            fetchDatabase(); // Update the database UI after saving location
        } else {
            alert('Failed to save location');
        }
    } catch (error) {
        alert('An error occurred while saving location');
        console.error(error);
    }
});

document.getElementById('retrieveForm').addEventListener('submit', async (e) => {
    e.preventDefault();
    const userId = document.getElementById('retrieveUserId').value;

    try {
        const response = await fetch(`/get_location/${userId}`);
        if (response.ok) {
            const data = await response.json();
            const { location, timestamp } = data;
            document.getElementById('lastLocation').innerHTML = `<p>Location: ${location}</p><p>Timestamp: ${timestamp}</p>`;
        } else if (response.status === 404) {
            alert('User not found');
        } else {
            alert('Failed to retrieve location');
        }
    } catch (error) {
        alert('An error occurred while retrieving location');
        console.error(error);
    }
});

// Fetch the database on page load
fetchDatabase();